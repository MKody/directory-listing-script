<?php
/*
  Voici donc la configuration du "Directory Listing Script"
  Si ce n'est pas fait, copiez (oui, COPIEZ et non renommez)
  ce fichier en "config.exemple.php" en "config.php" dans le
  dossier "dlf" !
*/


/*
  Titre de la page - Ce gros texte qui s'affiche en haut et
  dans le titre du site. (&hearts; est un coeur)
*/
$titrepage = 'Directory Script &hearts;';

/*
  Dossier de départ - Mettez un point si vous voulez commancer
  depuis le dossier actuel, ou avec un chemin relatif au dossier
  que vous souhaitez afficher dès le départ.
*/
$startdir = '.';

/*
  Afficher les minatures ? - Si vous mettez ceci sur "true",
  des miniatures seront affichés lors du passe de la souris sur
  les images.
  Vous devez avoir GD2 installé.
*/
$showthumbnails = true;

/*
  Limiteur de mémoire - Le génération des miniatures peut prendre
  une taille de mémoire (RAM) plus élevée que défini dans le PHP.INI
  avec des images plus larges. Si vous utilisez une machine partagée
  ou un hébergement web classique, précisez la taille de RAM que vous
  souhaitez attribuer au maximum.
*/
$memorylimit = false; // Integrer

/*
  Affichier les dossier - Voulez-vous faire afficher les sous-dossiers ?
  Si vous ne le voulez pas, mettez "true" sur "false".
*/
$showdirs = true;

/*
  Forcer le téléchargement - Voulez-vous forcer le téléchargement des
  fichiers aux visiteurs au lieu qu'ils puissent les ouvrir dans leurs
  navigateur ? (Vous pouvez aussi utiliser les .htaccess)
*/
$forcedownloads = false;

/*
  Fichiés cachés - Si vous voulez cacher des fichiers , vous pouvez
  les ajouter en dessous de ceux déjà proposés, ou alors en enlever.
  ATTENTION: Si votre fichier à un bout de texte d'un élément en dessous,
  il sera caché aussi ! Aussi, il est bien de cacher le dossier 'dlf'.
  NOTE: Le dernier de la liste ne prends pas de virgule après ;)
*/
$hide = array(
    /* Dossier du script */
    'dlf',
    /* Stuff du serveur */
    '.htaccess',
    '.htpasswd',
    /* Fichiers "poubelles" des OS */
    'Thumbs',
    '.DS_Store',
    '._.',
    /* Git-Stuff */
    '.git',
    '.gitignore',
    'README.md',
    /* Le fichier index.php, vu qu'il est à l'accueil */
    'index.php'
);

/*
  Afficher uniquement avec l'extension... - Si vous voulez afficher que
  certains types de fichier, décommentez cette partie et modifiez à votre
  guise.
  NOTE: Le dernier de la liste ne prends pas de virgule après ;)
*/
/*$showtypes = array(
    'jpg',
    'png',
    'gif',
    'zip',
    'txt'
);*/

/*
  Activer les envois ? - Affichera un formulaire d'envoi.
  Activez ceci uniquement si vous avez protégé la page/dossier de ce
  script avec un mot de passe ou si vous êtes en local par sécurité !
*/
$allowuploads = false;

/*
  Types de fichier à envoyer - Si vous activez les envois et que
  vous ne voulez autoriser uniquement certains types de fichier,
  précisez-les dans la liste en dessous.
  (Retirez ceux proposés si nécessaire.)
*/
$uploadtypes = array(
    'png',
    'jpeg',
    'bmp',
    'jpg',
    'gif',
    'zip',
    'rar',
    'exe',
    'setup',
    'txt',
    'htm',
    'html',
    'fla',
    'swf',
    'xls',
    'xlsx',
    'doc',
    'docx',
    'sig',
    'fh10',
    'pdf',
    'psd',
    'rm',
    'mpg',
    'mpeg',
    'mov',
    'avi',
    'eps',
    'gz',
    'asc'
);

/*
  Ecraser les fichiers - Si lors d'un upload le fichier existe,
  voulez-vous qu'il soit remplacé par le nouveau envoyé ?
*/
$overwrite = false;

/*
  Ouvrir le fichier d'index - Si un fichier index (liste en dessous) est
  dans le dossier, l'ouvrir au lieu d'affichier le dossier.
  Utile si vous mettez un site dans un sous-dossier.
*/
$displayindex = true;

/*
  Fichiers d'index - Si vous laissez activé l'ouverture des fichiers
  (au dessus), les fichiers indiqués dans la liste suivante seront ouverts.
  Vous pouvez ajouter les index.php si nécessaire.
*/
$indexfiles = array(
    'index.html',
    'index.htm',
    'index.php',
    'default.htm',
    'default.html'
);

/*
  Icônes
*/
$filetypes = array(
    'png' => 'jpg.gif',
    'jpeg' => 'jpg.gif',
    'bmp' => 'jpg.gif',
    'jpg' => 'jpg.gif',
    'gif' => 'gif.gif',
    'zip' => 'archive.png',
    'rar' => 'archive.png',
    'exe' => 'exe.gif',
    'setup' => 'setup.gif',
    'txt' => 'text.png',
    'htm' => 'html.gif',
    'html' => 'html.gif',
    'fla' => 'fla.gif',
    'swf' => 'swf.gif',
    'xls' => 'xls.gif',
    'xlsx' => 'xls.gif',
    'doc' => 'doc.gif',
    'docx' => 'doc.gif',
    'sig' => 'sig.gif',
    'fh10' => 'fh10.gif',
    'pdf' => 'pdf.gif',
    'psd' => 'psd.gif',
    'rm' => 'real.gif',
    'mpg' => 'video.gif',
    'mpeg' => 'video.gif',
    'mov' => 'video2.gif',
    'avi' => 'video.gif',
    'eps' => 'eps.gif',
    'gz' => 'archive.png',
    'asc' => 'sig.gif'
);


/*
  Voilà ! Tout la config est au dessus.
*/
