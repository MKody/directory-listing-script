# Directory Listing Script

Hey ! Tu as surement vu mon acienne page de screen **[ICI](http://s.mkody.info/)** ou alors n'importe quelle page qui utilise un système pour afficher les fichiers dans un dossier. Et bah tu peux le retrouver **ici** !

Si tu veux un exemple fonctionnel avec des éléments et des dossiers, tu peux regarder là: [MKody.info/folders](http://mkody.info/folders/)

## Comment l'obtenir/installer ?
- Cliquez sur le nuage avec une flèche situé dans la barre et clique `Download repository` ou choissiez une version précise
- Décompressze les fichiers
- Envoize **tous** les fichiers *(`index.php` et le dossier `dlf` au complet !)* dans le dossier voulu
- Ouvrez dans votre navigateur la page qui contient le script, un fichier de config' sera copié
- (S'il a un problème avec les droits, il est explixé)
- Modifiez la configuration dans `dlf/config.php` si besoin
- ???
- **"profit!"**

**NOTE:** Si vous placez un `index.html`, `index.htm`, `default.html` OU `defautl.html`  dans un dossier, il chargera ce fichier !
Utile si vous placez un site dans un sous-dossier (Configurable dans `dlf/config.php`, `index.php` pas recommandé sauf dans un cas précis)

## J'ai un soucis !
Demande à mon oiseau bleu ici: **[@MKody97](http://twitter.com/MKody97)**…
Ou via les *issues* par **[ICI](https://bitbucket.org/MKody/directory-listing-script/issues/new)**~ (Il est bien aussi)
